﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlCommander
{
    public abstract class Commander
    {
        
        public static Commander? GetCommander(EnvEnum envEnum, string connectionString)
        {
            if(envEnum == EnvEnum.mysql)
            {
                return new MysqlCommander(connectionString);
            }else if(envEnum == EnvEnum.mssql)
            {
                return new MssqlCommander(connectionString);
            }
            throw new ArgumentException("Invalid Env argument");
        }
     
        public async Task<List<List<string>>> SelectWithPrint(string fields, string from, string? where = null)
        {
            var result = await Select(fields, from, where);
            PrintSelect(result);
            return result;
        } 

        public void PrintSelect(List<List<string>> selectResult)
        {
            Console.WriteLine($"ROW NUM {selectResult.Count}");
            var lineWidth = selectResult[0].Count;

            foreach(var row in selectResult)
            {
                foreach(var cell in row)
                {
                    lineWidth += cell.Length;
                }
            }
            foreach (var row in selectResult)
            {
                Console.WriteLine(string.Concat(Enumerable.Repeat("-", lineWidth)));
                var rowText = "|";
                foreach (var cell in row)
                {
                    rowText += cell.ToString() + "|";
                }

                Console.WriteLine(rowText);
            }
        }

        public abstract Task<List<List<string>>> Select(string fields, string from, string? where = null);
    }
}
