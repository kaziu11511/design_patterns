﻿using MySqlConnector;
using System.Data.SqlClient;

namespace SqlCommander
{
    public class MssqlCommander : Commander
    {
        readonly SqlConnection _sqlConnector;

        public MssqlCommander(string connectionString)
        {
            _sqlConnector = new SqlConnection(connectionString);
        }


        public override async Task<List<List<string>>> Select(string fields, string from, string? where = null)
        {
            string whereSql = string.IsNullOrWhiteSpace(where) ? string.Empty : $" where {where}";
            _sqlConnector.Open();
            var result = new List<List<string>>();

            try
            {
                using var command = new SqlCommand($"Select {fields} from {from} {whereSql}", _sqlConnector);
                using var reader = await command.ExecuteReaderAsync();
                while (reader.Read())
                {
                    var row = new List<string>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        row.Add(reader.GetString(i));
                    }

                    result.Add(row);
                }
            }
            finally
            {
                _sqlConnector.Close();
            }

            return result;
        }
    }
}
